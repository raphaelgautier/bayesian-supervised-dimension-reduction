from setuptools import setup

setup(
    name="BayesianSupervisedDimensionReduction",
    version="1.0",
    url="https://github.com/rhgautier/Bayesian_dimension_reduction",
    author="Raphael Gautier",
    author_email="raphael.gautier@gatech.edu",
    description='Github repository accompanying the paper entitled "A Fully Bayesian\
         Gradient-Free Supervised Dimension Reduction Method using Gaussian Processes".\
         ',
    packages=["bsdr"],
    install_requires=[],
)
