from configparser import ConfigParser
from pathlib import Path


config = ConfigParser()
config.read(Path(__file__).parents[1] / ".config")
data_dir = Path(config["data"]["data_dir"])
doe_scheduler_server = config["doe_scheduler"]["server"]
doe_name = config["doe_scheduler"]["doe_name"]
cases_list = list(
    range(
        int(config["doe_scheduler"]["cases_start"]),
        int(config["doe_scheduler"]["cases_end"]),
    )
)
