from h5py import File
from pandas import DataFrame
from tqdm import tqdm

from bsdr import data_dir

# List all files in the relevant directory
all_files = list((data_dir / "results" / "final").glob("**/*.h5"))

# Extract results from h5 files
rows = []
for file in tqdm(all_files):
    try:
        with File(file, "r") as h5_file:
            case_inputs = h5_file["case_inputs"].attrs
            training_duration = h5_file["training_artifacts"].attrs["training_duration"]
            metrics = h5_file["validation_artifacts"]["validation_set"].attrs

            if "subspace_metrics" in h5_file["validation_artifacts"]:
                mfsa = h5_file["validation_artifacts"]["subspace_metrics"].attrs[
                    "mean_fsa"
                ]
            else:
                mfsa = None

            rows.append(
                [
                    case_inputs["dataset_name"],
                    case_inputs["output_name"],
                    case_inputs["model_name"],
                    case_inputs["dim_feature_space"],
                    case_inputs["num_training_samples"],
                    case_inputs["dataset_split_random_seed"],
                    training_duration,
                    metrics["r_squared"],
                    metrics["mlppd"],
                    mfsa,
                ]
            )
    except Exception as e:
        print(e)
        print(f"Problem with file {file}")

# Create a dataframe
df = DataFrame(
    data=rows,
    columns=[
        "dataset_name",
        "output_name",
        "model_name",
        "dim_feature_space",
        "num_training_samples",
        "dataset_split_random_seed",
        "training_duration",
        "r_squared",
        "mlppd",
        "mfsa",
    ],
)

# Save compiled results as csv
df.to_csv(data_dir / "results" / "final" / "compiled_results.csv")
