from itertools import product
from json import dump

from h5py import File

from bsdr import data_dir
from bsdr.utils import generate_reproducible_dataset_split

# DOE Parameters #######################################################################

# doe_name = "reruns_moas_500restarts"
# datasets = {
#     "CRM_450kgrid_twist_50DV_transonic": {
#         "dim_feature_space": [1, 2, 3, 4, 5],
#         "output_names": [
#             "lift",
#             "drag",
#             # "efficiency",
#             # "force_x",
#             # "force_y",
#             # "force_z",
#             "moment_x",
#             "moment_y",
#             "moment_z",
#             "sideforce",
#         ],
#     },
#     "quadratic_function_10_inputs_1_active_dims": {
#         "dim_feature_space": [1],
#         "output_names": ["y"],
#     },
#     "quadratic_function_10_inputs_2_active_dims": {
#         "dim_feature_space": [2],
#         "output_names": ["y"],
#     },
#     "quadratic_function_10_inputs_5_active_dims": {
#         "dim_feature_space": [5],
#         "output_names": ["y"],
#     },
#     "quadratic_function_25_inputs_1_active_dims": {
#         "dim_feature_space": [1],
#         "output_names": ["y"],
#     },
#     "quadratic_function_25_inputs_2_active_dims": {
#         "dim_feature_space": [2],
#         "output_names": ["y"],
#     },
#     "quadratic_function_25_inputs_5_active_dims": {
#         "dim_feature_space": [5],
#         "output_names": ["y"],
#     },
#     "quadratic_function_50_inputs_1_active_dims": {
#         "dim_feature_space": [1],
#         "output_names": ["y"],
#     },
#     "quadratic_function_50_inputs_2_active_dims": {
#         "dim_feature_space": [2],
#         "output_names": ["y"],
#     },
#     "quadratic_function_50_inputs_5_active_dims": {
#         "dim_feature_space": [5],
#         "output_names": ["y"],
#     },
#     "quadratic_function_100_inputs_1_active_dims": {
#         "dim_feature_space": [1],
#         "output_names": ["y"],
#     },
#     "quadratic_function_100_inputs_2_active_dims": {
#         "dim_feature_space": [2],
#         "output_names": ["y"],
#     },
#     "quadratic_function_100_inputs_5_active_dims": {
#         "dim_feature_space": [5],
#         "output_names": ["y"],
#     },
#     "elliptic_pde": {"dim_feature_space": [1, 2, 3, 4, 5], "output_names": ["y_long"]},
#     "hiv": {"dim_feature_space": [1, 2, 3, 4, 5], "output_names": ["y_3400"]},
#     "naca0012": {
#         "dim_feature_space": [1, 2, 3, 4, 5],
#         "output_names": ["lift", "drag"],
#     },
#     "onera_m6": {
#         "dim_feature_space": [1, 2, 3, 4, 5],
#         "output_names": ["lift", "drag"],
#     },
#     "CRM_450kgrid_twist_50DV_subsonic": {
#         "dim_feature_space": [1, 2, 3, 4, 5],
#         "output_names": [
#             "lift",
#             "drag",
#             "moment_x",
#             "moment_y",
#             "moment_z",
#             "sideforce",
#         ],
#     },
#     "RAE2822_baseline_51DV_M0.725": {
#         "dim_feature_space": [1, 2, 3, 4, 5],
#         "output_names": ["lift", "drag", "moment_z"],
#     },
# }
# # alt_model_names = ["bfs", "moas", "bgp"]
# alt_model_names = ["moas"]
# # alt_dim_feature_space = [1, 2, 3, 4, 5]
# alt_num_training_samples_multiplicator = [1, 2, 3, 4, 5]
# alt_dataset_split_random_seeds = [867, 57, 353, 601, 802]

doe_name = "oneram6_longer_chains"
datasets = {
    "onera_m6": {"dim_feature_space": [1, 5], "output_names": ["lift"]},
}
alt_model_names = ["bfs"]
alt_num_training_samples_multiplicator = [2, 5]
alt_dataset_split_random_seeds = [867]

# Process Paramaters ###################################################################

# Parameters used for MCMC (BFS and BGP)
# MCMC_PARAMETERS = {
#     "target_acceptance_probability": 0.8,
#     "num_chains": 1,
#     "chain_method": "parallel",
#     "num_warmup_draws": 500,
#     "num_posterior_draws": 1000,
#     "random_seed": 0,
#     "progress_bar": True,
#     "display_summary": True,
# }

MCMC_PARAMETERS = {
    "target_acceptance_probability": 0.8,
    "num_chains": 1,
    "chain_method": "parallel",
    "num_warmup_draws": 5000,
    "num_posterior_draws": 15000,
    "random_seed": 0,
    "progress_bar": False,
    "display_summary": True,
}

# Parameters used for manifold optimization (MOAS)
PYMANOPT_SOLVER_PARAMS = {
    "maxiter": 5000,
    "minstepsize": 1e-5,
    "mingradnorm": 2e-1,
    "cost_improvement_threshold": 1e-3,
    "no_cost_improvement_streak": 50,
}

# Process parameters
PROCESS_PARAMETERS = {
    "training_parameters": {
        "bfs": {"mcmc_params": MCMC_PARAMETERS},
        "moas": {
            "num_restarts": 500,
            "random_seed_for_restarts": 0,
            "pymanopt_solver_params": PYMANOPT_SOLVER_PARAMS,
        },
        "bgp": {"mcmc_params": MCMC_PARAMETERS},
    },
    "validation_parameters": {
        "confidence_interval_bounds_cdf_values": [0.025, 0.975],
        "num_gp_samples": 15,
        "random_seed": 0,
    },
}

# DOE Generation #######################################################################
cases = {}
case_number = 0

for dataset_name, dataset_info in datasets.items():
    # Retrieve needed dataset metadata
    dataset_outputs = dataset_info["output_names"]
    with File(data_dir / "datasets" / f"{dataset_name}.h5", "r") as dataset:
        num_samples = dataset["inputs"].shape[0]
        num_inputs = dataset["inputs"].shape[1]
        output_names = list(dataset["outputs"].keys())

    # Generate all cases for this dataset
    for (
        model_name,
        dim_feature_space,
        num_training_samples_multiplicator,
        dataset_split_random_seed,
        output_name,
    ) in product(
        alt_model_names,
        dataset_info["dim_feature_space"],
        alt_num_training_samples_multiplicator,
        alt_dataset_split_random_seeds,
        dataset_outputs,
    ):
        num_training_samples = int(num_inputs * num_training_samples_multiplicator)
        training_indices, validation_indices = generate_reproducible_dataset_split(
            dataset_split_random_seed, num_training_samples, num_samples,
        )
        name = "{}_{}_{}_{}dFS_{}TS_RS{}".format(
            model_name,
            dataset_name,
            output_name,
            dim_feature_space,
            num_training_samples,
            dataset_split_random_seed,
        )

        cases[case_number] = {
            "name": name,
            "model_name": model_name,
            "num_training_samples": num_training_samples,
            "dim_feature_space": dim_feature_space,
            "dataset_split_random_seed": dataset_split_random_seed,
            "dataset_name": dataset_name,
            "output_name": output_name,
            "training_indices": training_indices.tolist(),
        }
        case_number += 1

doe = {"process_parameters": PROCESS_PARAMETERS, "cases": cases}

print(f"This DOE contains {len(cases)} cases.")
with open(data_dir / "does" / f"{doe_name}.json", "w") as doe_file:
    dump(doe, doe_file, indent=2)
