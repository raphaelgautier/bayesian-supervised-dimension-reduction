from json import load

from bsdr import data_dir
from bsdr.run_case import run_case


if __name__ == "__main__":
    doe_name, case_number = "crm_transonic", 1500

    # Retrieve info
    with open(data_dir / "does" / f"{doe_name}.json", "r") as doe_file:
        doe = load(doe_file)
        process_parameters = doe["process_parameters"]
        case_parameters = doe["cases"][str(case_number)]

    # Run the case
    run_case(doe_name, case_number, process_parameters, case_parameters)
