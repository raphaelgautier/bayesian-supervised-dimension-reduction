from bsdr.run_case import run_case
from bsdr.utils import generate_reproducible_dataset_split, load_dataset

# Process Paramaters ###################################################################

# Parameters used for MCMC (BFS and BGP)
MCMC_PARAMETERS = {
    "target_acceptance_probability": 0.8,
    "num_chains": 1,
    "chain_method": "parallel",
    "num_warmup_draws": 500,
    "num_posterior_draws": 1000,
    "random_seed": 0,
    "progress_bar": True,
    "display_summary": True,
}

# Parameters used for manifold optimization (MOAS)
PYMANOPT_SOLVER_PARAMS = {
    "maxiter": 5000,
    "minstepsize": 1e-5,
    "mingradnorm": 2e-1,
    "cost_improvement_threshold": 1e-3,
    "no_cost_improvement_streak": 50,
}

# Process parameters
PROCESS_PARAMETERS = {
    "training_parameters": {
        "bfs": {"mcmc_params": MCMC_PARAMETERS},
        "moas": {
            "num_restarts": 100,
            "random_seed_for_restarts": 0,
            "pymanopt_solver_params": PYMANOPT_SOLVER_PARAMS,
        },
        "bgp": {"mcmc_params": MCMC_PARAMETERS},
    },
    "validation_parameters": {
        "confidence_interval_bounds_cdf_values": [0.025, 0.975],
        "num_gp_samples": 15,
        "random_seed": 0,
    },
}

# DOE name
DOE_NAME = "no_doe"

if __name__ == "__main__":

    for model_name in [
        "bfs",
        "moas",
        "bgp",
    ]:

        # Case Parameters ##############################################################
        num_training_samples = 40
        dataset_split_random_seed = 0
        dim_feature_space = 1
        dataset_name = "naca0012"
        output_name = "lift"
        name = "{}_{}_{}_{}TS_{}dFS".format(
            model_name,
            dataset_name,
            output_name,
            num_training_samples,
            dim_feature_space,
        )

        # Split dataset between training and validation
        x, _, _ = load_dataset(dataset_name, output_name)
        training_indices, _ = generate_reproducible_dataset_split(
            dataset_split_random_seed, num_training_samples, x.shape[0]
        )

        CASE_PARAMETERS = {
            "name": name,
            "model_name": model_name,
            "num_training_samples": num_training_samples,
            "dataset_split_random_seed": dataset_split_random_seed,
            "dim_feature_space": dim_feature_space,
            "dataset_name": dataset_name,
            "output_name": output_name,
            "training_indices": training_indices,
        }

        run_case(DOE_NAME, 0, PROCESS_PARAMETERS, CASE_PARAMETERS)
