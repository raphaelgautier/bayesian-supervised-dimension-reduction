__package__ = "scripts.plotting.parametric_study"

import pandas as pd

from bsdr import data_dir

from .plotting_routine import plot_metric

# Path to results file
file_path = data_dir / "results" / "final" / "compiled_results.csv"

# Read results file
results_df = pd.read_csv(file_path, index_col=0)

metrics = [
    "training_duration",
    "r_squared",
    "mlppd",
    "mfsa",
]

for metric in metrics:
    plot_metric(metric, results_df)
