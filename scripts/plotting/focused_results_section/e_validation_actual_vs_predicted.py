import h5py
from matplotlib import pyplot as plt
import numpy as np
import seaborn as sns

from bsdr import data_dir
from bsdr.utils import plot_actual_vs_predicted_asymmetric

# Script Parameters
subfolder = "oneram6_longer_chains"
dataset_name = "onera_m6"
output_name = "lift"
cases = [
    "bfs_onera_m6_lift_1dFS_100TS_RS867",
    "bfs_onera_m6_lift_1dFS_250TS_RS867",
    "bfs_onera_m6_lift_5dFS_100TS_RS867",
    "bfs_onera_m6_lift_5dFS_250TS_RS867",
]
num_max_disp_proj_params = 5
num_prior_samples = 1000

param_labels = {
    "noise_variance": "\\sigma_n",
    "signal_variance": "\\sigma_f",
    "length_scale_0": "\\ell_0",
    "length_scale_1": "\\ell_1",
    "length_scale_2": "\\ell_2",
    "length_scale_3": "\\ell_3",
    "length_scale_4": "\\ell_4",
    "projection_parameter_0": "\\theta_{{p,0}}",
    "projection_parameter_1": "\\theta_{{p,1}}",
    "projection_parameter_2": "\\theta_{{p,2}}",
    "projection_parameter_3": "\\theta_{{p,3}}",
    "projection_parameter_4": "\\theta_{{p,4}}",
}

# Matplotlib settings
plt.rcParams.update({"font.size": 8, "text.usetex": False, "font.serif": "Arial"})
sns.set(style="ticks")
FONT_SIZE = 8
plt.rc("font", size=FONT_SIZE)  # controls default text sizes
plt.rc("axes", titlesize=FONT_SIZE)  # fontsize of the axes title
plt.rc("axes", labelsize=FONT_SIZE)  # fontsize of the x and y labels
plt.rc("xtick", labelsize=FONT_SIZE)  # fontsize of the tick labels
plt.rc("ytick", labelsize=6)  # fontsize of the tick labels
plt.rc("legend", fontsize=FONT_SIZE)  # legend fontsize
plt.rc("figure", titlesize=FONT_SIZE)  # fontsize of the figure title

# Create one plot for each case
for case in cases:

    # Open hdf5 files
    results_file_path = data_dir / "results" / subfolder / dataset_name / f"{case}.h5"
    results_file = h5py.File(results_file_path, "r")

    dataset_file_path = data_dir / "datasets" / f"{dataset_name}.h5"
    dataset_file = h5py.File(dataset_file_path, "r")

    # Retrieve predicted with confidence interval
    predicted = np.array(
        results_file["validation_artifacts"]["validation_set"][
            "point_based_predictions"
        ]
    )[:, 0]
    lower_bound = np.array(
        results_file["validation_artifacts"]["validation_set"][
            "confidence_interval_bounds"
        ][:, 0]
    )
    upper_bound = np.array(
        results_file["validation_artifacts"]["validation_set"][
            "confidence_interval_bounds"
        ][:, 1]
    )

    # Retriecve actual
    training_indices = np.array(results_file["case_inputs"].attrs["training_indices"])
    num_total_samples = dataset_file["outputs"][output_name].shape[0]
    all_indices = np.arange(num_total_samples)
    validation_indices = np.setdiff1d(all_indices, training_indices)
    actual = np.array(dataset_file["outputs"][output_name])[validation_indices, 0]

    # Close files
    results_file.close()
    dataset_file.close()

    # Plot
    fig, ax = plt.subplots(1, 1, figsize=(3, 3))
    plot_actual_vs_predicted_asymmetric(
        actual, predicted, lower_bound, upper_bound, title="", ax=ax
    )
    plt.savefig(
        data_dir
        / "figures"
        / subfolder
        / "validation_actual_vs_predicted"
        / f"{case}.pdf"
    )
