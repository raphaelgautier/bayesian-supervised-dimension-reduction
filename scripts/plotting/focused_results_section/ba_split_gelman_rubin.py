import h5py
import numpy as np
from numpyro.diagnostics import summary
import pandas as pd
from tqdm import tqdm

from bsdr import data_dir

# Parameters
dataset_name = "onera_m6"
output_name = "lift"

# Compute split Gelman-Rubin statistic for each case
dir_path = data_dir / "results" / "final" / dataset_name
files = list(dir_path.glob(f"bfs_{dataset_name}_{output_name}_*.h5"))
rows = []
for file_path in tqdm(files):
    with h5py.File(file_path, "r") as h5_file:
        posterior_draws = h5_file["training_artifacts"]["posterior_draws"]
        posterior_draws = {
            key: np.array(value) for key, value in posterior_draws.items()
        }
        statistics = summary(posterior_draws, group_by_chain=False)
        row = {
            "dataset_name": dataset_name,
            "output_name": output_name,
            "dim_feature_space": h5_file["case_inputs"].attrs["dim_feature_space"],
            "num_training_samples": h5_file["case_inputs"].attrs[
                "num_training_samples"
            ],
        }
        for key, value in statistics.items():
            if np.isscalar(value["r_hat"]):
                row[key] = value["r_hat"]
            else:
                for i in range(min(len(value["r_hat"]), 5)):
                    row[f"{key}_{i}"] = value["r_hat"][i]
        rows.append(row)
pd.DataFrame(rows).to_csv(
    data_dir / "figures" / "b_split_gelman_rubin" / f"{dataset_name}_{output_name}.csv"
)
