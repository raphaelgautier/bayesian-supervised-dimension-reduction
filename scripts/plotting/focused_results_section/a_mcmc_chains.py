import h5py
from matplotlib import pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import numpy as np
import seaborn as sns

from bsdr import data_dir

# Parameters
subfolder = "oneram6_longer_chains"
dataset_name = "onera_m6"
case_names = [
    "bfs_onera_m6_lift_1dFS_100TS_RS867",
    "bfs_onera_m6_lift_1dFS_250TS_RS867",
    "bfs_onera_m6_lift_5dFS_100TS_RS867",
    "bfs_onera_m6_lift_5dFS_250TS_RS867",
]
num_projection_parameters = 5

for case_name in case_names:

    # Asemble file path
    file_path = data_dir / "results" / subfolder / dataset_name / f"{case_name}.h5"

    # Matplotlib
    plt.rcParams.update({"font.size": 8, "text.usetex": False, "font.serif": "Arial"})
    sns.set(style="ticks")
    FONT_SIZE = 8
    plt.rc("font", size=FONT_SIZE)  # controls default text sizes
    plt.rc("axes", titlesize=FONT_SIZE)  # fontsize of the axes title
    plt.rc("axes", labelsize=FONT_SIZE)  # fontsize of the x and y labels
    plt.rc("xtick", labelsize=FONT_SIZE)  # fontsize of the tick labels
    plt.rc("ytick", labelsize=6)  # fontsize of the tick labels
    plt.rc("legend", fontsize=FONT_SIZE)  # legend fontsize
    plt.rc("figure", titlesize=FONT_SIZE)  # fontsize of the figure title

    # Prepare plot
    with h5py.File(file_path, "r") as h5_file:

        # Retrieve chains
        posterior_draws = h5_file["training_artifacts"]["posterior_draws"]
        signal_variance = np.array(posterior_draws["signal_variance"])
        noise_variance = np.array(posterior_draws["noise_variance"])
        length_scales = np.array(posterior_draws["length_scales"])
        projection_parameters = np.array(
            posterior_draws["projection_parameters"][:, :num_projection_parameters]
        )
        num_draws = projection_parameters.shape[0]

        # Make sure length scales is a 2D array
        if len(length_scales.shape) < 2:
            length_scales = length_scales[:, None]

        # Total number of rows
        num_rows = 2 + length_scales.shape[1] + projection_parameters.shape[1]

        # Figure
        plt.figure(figsize=(3, 3.5))

        # Signal variance
        ax = plt.subplot(num_rows, 1, 1)
        ax.plot(signal_variance, linewidth=0.5)
        plt.ylabel("$\\sigma_f$")
        plt.xticks([], [])
        plt.yticks([np.min(signal_variance), np.max(signal_variance)])
        plt.xlim(0, num_draws)
        ax.yaxis.set_major_formatter(FormatStrFormatter("%.2f"))

        # Noise variance
        ax = plt.subplot(num_rows, 1, 2)
        ax.plot(noise_variance, linewidth=0.5)
        plt.ylabel("$\\sigma_n$")
        plt.xticks([], [])
        plt.yticks([np.min(noise_variance), np.max(noise_variance)])
        plt.xlim(0, num_draws)
        ax.yaxis.set_major_formatter(FormatStrFormatter("%.2f"))

        # Length scales
        for i in range(length_scales.shape[1]):
            ax = plt.subplot(num_rows, 1, 3 + i)
            ax.plot(length_scales[:, i], linewidth=0.5)
            plt.ylabel(f"$\\ell_{i+1}$")
            plt.xticks([], [])
            plt.yticks([np.min(length_scales[:, i]), np.max(length_scales[:, i])])
            plt.xlim(0, num_draws)
            ax.yaxis.set_major_formatter(FormatStrFormatter("%.2f"))

        # Projection parameters
        for i in range(projection_parameters.shape[1]):
            ax = plt.subplot(num_rows, 1, 3 + length_scales.shape[1] + i)
            ax.plot(projection_parameters[:, i], linewidth=0.5)
            plt.ylabel(f"$\\theta_{{p,{i+1}}}$")
            plt.xticks([], [])
            plt.yticks(
                [
                    np.min(projection_parameters[:, i]),
                    np.max(projection_parameters[:, i]),
                ]
            )
            plt.xlim(0, num_draws)
            ax.yaxis.set_major_formatter(FormatStrFormatter("%.2f"))

        plt.tight_layout()
        plt.subplots_adjust(hspace=0.2)

    plt.savefig(data_dir / "figures" / subfolder / "mcmc_chains" / f"{case_name}.pdf")
    # plt.show()
