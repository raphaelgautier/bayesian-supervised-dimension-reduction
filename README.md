# A Fully Bayesian Gradient-Free Supervised Dimension Reduction Method using Gaussian Processes

Github repository accompanying the paper entitled "A Fully Bayesian Gradient-Free Supervised Dimension Reduction Method using Gaussian Processes".

## Setup

The setup instructions assume you are using anaconda/miniconda. Please note that the implementations of all models heavily rely on [Google's JAX framework](https://github.com/google/jax), which is only supported on Linux/MacOS/WSL. 

 - clone this repository and `cd` to the project root
 - create a conda environment using the provided `environment.yml` file (`conda env create --file=environment.yml`)
 - activate this environment (its default name is `bayesian_dimension_reduction`)
 - install this project as a library (`pip install -e .`)
 - finally, clone and install [this fork of pymanopt](https://gitlab.com/raphael.gautier/pymanopt)

## Run the code

The file `.config.example` should be copied as `.config` and the relevant configuration parameters should be filled. Configuration parameters under `[doe_scheduler]` can be ignored if only standalone cases are run instead of a DOE. The `[data]/data_dir` parameter should point to a directory that contains the data associated with this code. For example, you may use the data directory located [here](https://gitlab.com/raphaelgautier/bayesian-supervised-dimension-reduction-data). That repository contains all the data generated and used to create the plots presented in the paper.

The script `run_one_case.py` is the main point of entry. It features a set of parameters that can be modified to run a particular case. Running this script produces an `hdf5` file in a subfolder of the `results` directory that contains all relevant training and validation artifacts. `hdf5` files may be programatically opened, e.g. using [h5py](https://www.h5py.org/) or browsed using a utility such as [HDFView](https://www.hdfgroup.org/downloads/hdfview/).

## Repository Content

```shell
.
├── bsdr
│   ├── __init__.py         # config file is read here 
│   ├── bfs.py              # implementation of the proposed approach
│   ├── bgp.py              # implementation of a fully Bayesian GP
│   ├── moas.py             # implementation of the MO-AS method
│   ├── run_case.py         # script used to run a DOE case
│   └── utils.py            # library file containing utility functions
├── scripts                 
│   ├── doe                 # scripts used to run parametric studies
│   ├── example                 
│   │   └── run_one_case.py # simple example, start here
│   └── plotting            # scripts used to generate figures
├── .config.example         # copy to `.config` and edit
├── .gitignore              # gitignore
├── environment.yml         # use to install the relevant environment
├── README.md               # this file
└── setup.py                # install as a library (required to run scripts)
```
